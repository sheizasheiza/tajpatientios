package test;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class MedicalProfile extends BaseClass{
	
		public ExtentTest test;
@Test(priority=1,enabled=true)
public void clickOnMenuIcon(){
	test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
	test.log(Status.INFO,"Test case Started");	

	driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
	test.log(Status.INFO, "clicked Menu Icon on Home Screen");
}
@Test(priority=2,enabled=true)
public void tapOnMedicalprofile(){
	test =extent.createTest("CLICK MEDICAL PROFILE","This Test Case will click on medical profile.");
	test.log(Status.INFO,"Test case Started");
	driver.findElement(MobileBy.AccessibilityId("medical profile")).click();
	test.log(Status.PASS, "Medical profile clicked");
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
public void BacktoTab1() {
MobileElement tab1 =driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[1]"));
ActionMethods.WaitByvisibilityOfElement(tab1,2000);
ActionMethods.tapByElement(tab1);
test.log(Status.PASS, "Clicked on Personal profile");
}
public void ClickSaveButton() {
	MobileElement savebtn =driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Save\"]"));
	ActionMethods.WaitByvisibilityOfElement(savebtn, 5000);
	ActionMethods.tapByElement(savebtn);
	test.log(Status.PASS, "Save button clicked");
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
public void ClickCancleButton() {
	MobileElement cancelbtn =driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Cancel \"]"));
	ActionMethods.WaitByvisibilityOfElement(cancelbtn, 8000);
	ActionMethods.tapByElement(cancelbtn);
	test.log(Status.PASS, "Clicked on Cancel Button");
}
@Test(priority=3, enabled=true)
public void personalProfileSetHeight() throws InterruptedException{	

	test =extent.createTest("PERSONAL PROFILE SET HEIGHT","This Test Case will set the height of patient.");
	test.log(Status.INFO,"Test case Started");
	Thread.sleep(3000);
	MobileElement height =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"height\"]");
	 ActionMethods.WaitByvisibilityOfElement(height, 2000);
	 ActionMethods.tapByElement(height);
	test.log(Status.PASS, "Clicked on Height");
	MobileElement picker=	driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"));	
 // MobileElement picker =driver.findElementByClassName("XCUIElementTypePickerWheel");
	ActionMethods.WaitByvisibilityOfElement(picker, 2000);
	ActionMethods.scrollTwoStepsDown(picker);
	test.log(Status.PASS, "Select value from list");
	ClickSaveButton();
	BacktoTab1();
	
}
@Test(priority=4,enabled=true)
public void verifyCancelButtonOnSetHeight(){	
	
	test =extent.createTest("SET HEIGHT CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
	test.log(Status.INFO,"Test case Started");
	MobileElement height =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"height\"]");
	 ActionMethods.WaitByvisibilityOfElement(height, 2000);
	 ActionMethods.tapByElement(height);
	test.log(Status.PASS, "Clicked on height");
	ClickCancleButton();

}
@Test(priority=5, enabled=true)
public void personalProfileSetWeight() throws InterruptedException{	
	test =extent.createTest("PERSONAL PROFILE SET Weight","This Test Case will set the weight of patient.");
	test.log(Status.INFO,"Test case Started");
	Thread.sleep(3000);
	MobileElement weight =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"weight\"]");
	 ActionMethods.WaitByvisibilityOfElement(weight, 2000);
	 ActionMethods.tapByElement(weight);
	test.log(Status.PASS, "Clicked on Weight");
	MobileElement picker=	driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"));	
	ActionMethods.WaitByvisibilityOfElement(picker, 2000);
	ActionMethods.scrollTwoStepsDown(picker);
	test.log(Status.PASS, "Select value from list");
	ClickSaveButton();
	BacktoTab1();
}
@Test(priority=6,enabled=true)
public void verifyCancelButtonOnSetWeight(){	
	test =extent.createTest("SET WEIGHT CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
	test.log(Status.INFO,"Test case Started");
	MobileElement weight =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"weight\"]");
	 ActionMethods.WaitByvisibilityOfElement(weight, 2000);
	 ActionMethods.tapByElement(weight);
	test.log(Status.PASS, "Clicked on weight");
	ClickCancleButton();
}
@Test(priority=7, enabled=true)
public void personalProfileSetBloodGroup() throws InterruptedException{	

	test =extent.createTest("PERSONAL PROFILE SET Weight","This Test Case will set the weight of patient.");
	test.log(Status.INFO,"Test case Started");
	Thread.sleep(3000);
	MobileElement bloodGroup =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"blood group\"]");
	 ActionMethods.WaitByvisibilityOfElement(bloodGroup, 2000);
	 ActionMethods.tapByElement(bloodGroup);
	test.log(Status.PASS, "Clicked on Weight");
	MobileElement picker=	driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"));	
	ActionMethods.WaitByvisibilityOfElement(picker, 2000);
	ActionMethods.scrollOneStepUP(picker);
	test.log(Status.PASS, "Select value from list");
	ClickSaveButton();
	BacktoTab1();

}
@Test(priority=8,enabled=true)
public void verifyCancelButtonOnSetBloodGroup(){	
	
	test =extent.createTest("SET BLOOD GROUP CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
	test.log(Status.INFO,"Test case Started");
	MobileElement bloodGroup =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"blood group\"]");
	 ActionMethods.WaitByvisibilityOfElement(bloodGroup, 2000);
	 ActionMethods.tapByElement(bloodGroup);
	test.log(Status.PASS, "Clicked on Blood Group");
	ClickCancleButton();
	
	
}
@Test(priority=9, enabled=true)
public void personalProfileSetMaritalStatus() throws InterruptedException{	

	test =extent.createTest("PERSONAL PROFILE SET Weight","This Test Case will set the weight of patient.");
	test.log(Status.INFO,"Test case Started");
	Thread.sleep(3000);
	MobileElement status=driver.findElementByXPath("//XCUIElementTypeButton[@name=\"marital status\"]");
	 ActionMethods.WaitByvisibilityOfElement(status, 2000);
	 ActionMethods.tapByElement(status);
	test.log(Status.PASS, "Clicked on Weight");
	MobileElement picker=	driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"));	
	ActionMethods.WaitByvisibilityOfElement(picker, 2000);
	ActionMethods.scrollTwoStepsDown(picker);
	test.log(Status.PASS, "Select value from list");
	ClickSaveButton();
	BacktoTab1();
}

@Test(priority=10,enabled=true)
public void verifyCancelButtonOnSetMaritalStatus(){	
	
	test =extent.createTest("SET MARITAL STATUS CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
	test.log(Status.INFO,"Test case Started");
	MobileElement status=driver.findElementByXPath("//XCUIElementTypeButton[@name=\"marital status\"]");
	 ActionMethods.WaitByvisibilityOfElement(status, 2000);
	 ActionMethods.tapByElement(status);
	test.log(Status.PASS, "Clicked on MARITAL STATUS");
	ClickCancleButton();
}
@Test(priority=11, enabled=true)
public void LastVistTimePicker() throws InterruptedException {
	test =extent.createTest("SET LAST VIST DATE","This Test Case will set the Last Visit date to doctor.");
	test.log(Status.INFO,"Test case Started");
	MobileElement element= driver.findElement(By.xpath("//XCUIElementTypeTextField[@name=\"timepickerview\"]"));
	ActionMethods.WaitByvisibilityOfElement(element, 5000);
	ActionMethods.tapByElement(element);
	test.log(Status.PASS, "Clicked on last visit date");
	
	//element.sendKeys("02-11-2020");
//	MobileElement date =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther"));
//	test.log(Status.PASS, "Entered Last visit date");
//	ActionMethods.WaitByvisibilityOfElement(date, 5000);
//	ActionMethods.tapByElement(date);
	//ActionMethods.tapByCoordinates(5, 702);
	Thread.sleep(3000);
MobileElement savebtn= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Done\"]"));
	ActionMethods.WaitByvisibilityOfElement(savebtn, 8000);
	ActionMethods.tapByElement(savebtn);
	test.log(Status.PASS, "Clicked on Done Button");
	BacktoTab1();
	test.log(Status.PASS, "Back toPersonal profile");
}
@Test(priority=12, enabled=true)
public void VerifytimePickerCancleButton() throws InterruptedException {
	
	test =extent.createTest("CANCLE BUTTON ON LAST VIST DATE","This Test Case will verfiy cancle button is functional.");
	test.log(Status.INFO,"Test case Started");
	MobileElement element= driver.findElement(By.xpath("//XCUIElementTypeTextField[@name=\"timepickerview\"]"));
	ActionMethods.WaitByvisibilityOfElement(element, 5000);
	ActionMethods.tapByElement(element);
	test.log(Status.PASS, "Clicked on last visit date");
MobileElement Canclebtn= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Cancel \"]"));
	ActionMethods.WaitByvisibilityOfElement(Canclebtn, 8000);
	ActionMethods.tapByElement(Canclebtn);
	test.log(Status.PASS, "Clicked on Cancle Button");

}
public void LifeStyle() {
	MobileElement tab2 =driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[2]"));
	ActionMethods.WaitByvisibilityOfElement(tab2,2000);
	ActionMethods.tapByElement(tab2);
	test.log(Status.PASS, "Back toPersonal profile");
	//XCUIElementTypeStaticText[@name="Do you smoke/use tobbaco?"]
	//XCUIElementTypeSwitch[@name="Do you smoke/use tobbaco?"]

	//XCUIElementTypeStaticText[@name="Have you traveled overseas in the past 2 months?"]
	//XCUIElementTypeSwitch[@name="Have you traveled overseas in the past 2 months?"]
}
public void HealthProblem() {
	
	MobileElement tab2 =driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[3]"));
	ActionMethods.WaitByvisibilityOfElement(tab2,2000);
	ActionMethods.tapByElement(tab2);
	//picker
	//XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel/XCUIElementTypeOther[1]
}
@Test(priority=13,enabled=true)
public void lifeStyle(){
	test =extent.createTest("ADD LIFESTYLE DATA","This test case will add lifestyle data of patient");
	test.log(Status.INFO,"Test case Started");
	MobileElement tab2 =driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[2]"));
	ActionMethods.WaitByvisibilityOfElement(tab2,2000);
	ActionMethods.tapByElement(tab2);
	
	//XCUIElementTypeStaticText[@name="Do you smoke/use tobbaco?"]

	//XCUIElementTypeStaticText[@name="Have you traveled overseas in the past 2 months?"]
	test.log(Status.PASS, "clicked on Life style tab");
}
@Test(priority=14,enabled=true)
public void lifeStyleOptions() throws InterruptedException{
	test =extent.createTest("CLICK ON LIFESTYLE SWITCH","This test case will on/off switch");
	test.log(Status.INFO,"Test case Started");
	driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name=\"Do you smoke/use tobbaco?\"]")).click();
	Thread.sleep(2000);
	test.log(Status.PASS, "Clicked on 1st Switch, if the toggle is ON it will turn if OFF and Vice versa");
	driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name=\"Have you traveled overseas in the past 2 months?\"]")).click();
	test.log(Status.PASS, "Clicked on 2nd Switch, if the toggle is ON it will turn if OFF and Vice versa");
	Thread.sleep(2000);
}
//XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel
//XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]

//HEALTH PROBLEM
	@Test(priority=15,enabled= true)
	public void healthProblems(){	
		test =extent.createTest("ADD HEALTH PROBLEMS DATA","This test case will Enter Health problems of patient");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[3]")).click();
		test.log(Status.PASS, "clicked Health Problem Tab");
	}
public void saveSeverity() {
MobileElement setSeverity = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"));
ActionMethods.scrollTwoStepUp(setSeverity);
test.log(Status.PASS, "Severity of problem is set");
MobileElement savebtn =driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Save\"]"));
ActionMethods.WaitByvisibilityOfElement(savebtn, 5000);
ActionMethods.tapByElement(savebtn);
test.log(Status.PASS, "Save button clicked");
try {
	Thread.sleep(2000);
} catch (InterruptedException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
}
	@Test(priority=16, enabled=true)
	public void healthProblemList(){	
		MobileElement problem1 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]"));
		ActionMethods.tapOnElement(problem1);
		test.log(Status.PASS, "First problem is selected from list @index=1");
		saveSeverity();	
		healthProblems();
		MobileElement problem3 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]"));
	    ActionMethods.tapOnElement(problem3);
		test.log(Status.PASS, "Third problem is selected from list @index=3");
		saveSeverity();
		healthProblems();
		MobileElement problem6 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]"));
		ActionMethods.tapOnElement(problem6);		   
		test.log(Status.PASS, "Sixth problem is selected from list @index=6");
		saveSeverity();
		healthProblems();
	}
	@Test(priority=17, enabled=true)
	public void VerifyCancleButtonHealthProblem() {
		test =extent.createTest("VERIFY CANCEL BUTTON ON HEALTH PROBLEM LIST","This Test Case will verify cancel Button is Functional.");
		test.log(Status.INFO,"Test case Started");
		HealthProblem();
		MobileElement problem1 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]"));
		ActionMethods.tapOnElement(problem1);
		test.log(Status.PASS, "First problem is selected from list @index=1");
		ClickCancleButton();
		
	}

	@Test(priority=18, enabled=true)
	public void allergiesTab(){	
		test =extent.createTest("ADD ALLERGIES DATA","This test case will Enter Allergies data for patient");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[5]")).click();
		test.log(Status.PASS, "Clicked on Allergies Tab");
	}
	@Test (priority=19, enabled=true)
		public void allergiesList() {
		MobileElement problem1 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]"));
        ActionMethods.tapOnElement(problem1);
		test.log(Status.PASS, "First problem is selected from list @index=1");
		saveSeverity();	
		
		MobileElement problem3 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]"));
	    ActionMethods.tapOnElement(problem3);
		test.log(Status.PASS, "Third problem is selected from list @index=3");
		saveSeverity();
	
		MobileElement problem6 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]"));
		ActionMethods.tapOnElement(problem6);		   
		test.log(Status.PASS, "Sixth problem is selected from list @index=6");
		saveSeverity();
		}
	@Test(priority=20, enabled=true)
	public void VerifyCancleButtonAlergies() {
		test =extent.createTest("VERIFY CANCEL BUTTON ON ALERGIES","This Test Case will verify cancel Button is Functional.");
		test.log(Status.INFO,"Test case Started");
		allergiesTab();
		MobileElement problem1 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]"));
		ActionMethods.tapOnElement(problem1);
		test.log(Status.PASS, "First problem is selected from list @index=1");
		ClickCancleButton();	
	}
	@Test(priority=21,enabled=true)
	public void ongoingTreatment (){
		test =extent.createTest("ADD ONGOING TREATMENT DATA","This test case will treatment data.");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[4]")).click();
		test.log(Status.PASS, "clicked on  Ongoing Treatment tab");
	}
	//@Test(priority=22,enabled=true)
	public void addOngoingTreatment() throws InterruptedException{
		//driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"add new\"]")).click();
		driver.findElement(MobileBy.AccessibilityId("add new")).click();
		test.log(Status.PASS, "clicked on  Add Button");
		//XCUIElementTypeStaticText[@name="No Medical History"]
		String medicineTextField="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1]";
		MobileElement medicineName=driver.findElement(By.xpath(medicineTextField));
		medicineName.sendKeys("panadol");
		test.log(Status.PASS, "Panadol is entered, Matching record returned");
		Thread.sleep(3000);
		String medicineListxpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeTable/XCUIElementTypeCell[1]";
	    MobileElement medicine= driver.findElement(By.xpath(medicineListxpath));
		ActionMethods.WaitByvisibilityOfElement(medicine, 3000);
		//ActionMethods.tapOnElement(medicine);
		medicine.click();
		test.log(Status.PASS, "First item is selected from list of record");
		test.log(Status.PASS, "Medicine Name added ");
        String MedicineDatexpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[2]";
        MobileElement medicineDate=driver.findElement(By.xpath(MedicineDatexpath));
        ActionMethods.tapByElement(medicineDate);
	    //medicineDate.clear();
		//MobileElement date =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther"));
 		//ActionMethods.WaitByvisibilityOfElement(date, 5000);
	    // ActionMethods.tapByElement(date);
		//date.click();
		ActionMethods.tapByCoordinates(5, 703);
	    MobileElement savebtn= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Done\"]"));
		ActionMethods.WaitByvisibilityOfElement(savebtn, 5000);
		ActionMethods.tapByElement(savebtn);
		Thread.sleep(2000);
		test.log(Status.PASS,"Entereddate for medicine");	
	}
	@Test(priority=22, enabled=true)
	public void saveOnGoingTreatment() throws InterruptedException {
			addOngoingTreatment();
		    ClickSaveButton();
		    test.log(Status.PASS,"Medicine entered successfully");	
	}
	@Test(priority=23,enabled=true)
	public void verifyCancelButtonAddTreatment() throws InterruptedException{
		    ongoingTreatment();
			addOngoingTreatment();
			ClickCancleButton();
	}
	@Test(priority=24, enabled=true)
	public void GoToHome() {
		MobileElement homebutton= driver.findElement(By.xpath(
				"//XCUIElementTypeNavigationBar[@name=\"Medical Profile\"]/XCUIElementTypeButton[2]"));
		ActionMethods.WaitByvisibilityOfElement(homebutton, 5);
		ActionMethods.tapByElement(homebutton);		
	}
}