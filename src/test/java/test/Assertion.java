package test;

import io.appium.java_client.MobileElement;

public class Assertion extends BaseClass {
	public boolean isElementDisplayed(MobileElement el){
	     try{
	        return el.isDisplayed();
	     }
	     catch(Exception e){
	        return false;
	     }
	}
	
	
	public void Main() {
MobileElement element = driver.findElementById("element id");
	boolean isElementVisible = isElementDisplayed(element);
	if(isElementVisible){
	   //element is visible
	}else{
	   //element is not visible
	}
	}
}