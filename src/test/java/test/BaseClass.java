package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterMethod;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseClass extends ExtentReport{

	 public static AppiumDriver<MobileElement> driver;
	 
	 @BeforeTest (alwaysRun= true)
	  public void setup() throws MalformedURLException {
	    DesiredCapabilities capabilities = new DesiredCapabilities();
	    
	   // IPone 8 simulator
	    capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "iOS");
	    capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "14.2");
	    capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
	    capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8");
	    capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
	    // capabilities.setCapability("udid","862FE3B8-C3EB-4469-9D21-D3E5F505E874");
	    // 8 plus simulatorUID
	    capabilities.setCapability("udid","A85BEF39-B6DA-4BA9-BBDB-B1446F2D88B8"); 
	    capabilities.setCapability(MobileCapabilityType.APP, "/Users/zubairaslam/Library/Developer/Xcode/DerivedData"+
	    		    "/AER_Health-fjbiettjumbxhxcxxxgduxrbpnal/Build/Products/Debug-iphonesimulator/AER QA.app");
	    //Simulator UDID
	    
	    //Real device UDID iphone 7 plus
	    //d5428d4c71027944659e05841a108afedbcdb720
	    //iphone5 9c3c3cb1164fdf9291d00c3b83d63e1b24a97e8f
	    //capabilities.setCapability("udid","d5428d4c71027944659e05841a108afedbcdb720");
//iphone 5	 capabilities.setCapability("udid","9c3c3cb1164fdf9291d00c3b83d63e1b24a97e8f");
//iphone7
//     capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "14.2");
//	 capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
//	 capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 7 Plus");
//	 capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
//	 capabilities.setCapability("udid","d5428d4c71027944659e05841a108afedbcdb720");
//	 capabilities.setCapability("bundleId", "com.taj.patient");
//	 capabilities.setCapability("xcodeOrgId", "8B922WS9A6");
//  capabilities.setCapability("xcodeSigningId", "sheiza.irfan@infiniun.com");
//  capabilities.setCapability(MobileCapabilityType.APP, "/Users/zubairaslam/Library/Developer/Xcode/DerivedData"+
//		    "/AER_Health-fjbiettjumbxhxcxxxgduxrbpnal/Build/Products/Debug-iphoneos/AER QA.app");
////  capabilities.setCapability("updatedWDABundleId", "com.test.bundleId.WebDriverAgentRunner");
//  capabilities.setCapability("updatedWDABundleId", "com.infiniun.WebDriverAgentLib");
//capabilities.setCapability("updatedWDABundleId", "com.sheiza.WebDriverAgentRunner.xctrunner");
  //capabilities.setCapability("useNewWDA", false);

        URL url = new URL("http://127.0.0.1:4723/wd/hub");
	    driver = new AppiumDriver<MobileElement>(url, capabilities);
	    driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
	   System.out.println("Driver initiated successfully");
	  }

	
	@AfterTest
	  public void tearDown() throws Exception {
	   driver.quit();
	  }
	}