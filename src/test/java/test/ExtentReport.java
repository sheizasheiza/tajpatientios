package test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReport {
	public ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public ExtentTest test;
	@BeforeSuite
	public void reportSetup() {
		htmlReporter = new ExtentHtmlReporter("extent.html");
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setDocumentTitle("Taj Patient Reports");
		htmlReporter.config().setReportName("Automation Test Results");
		htmlReporter.config().setTheme(Theme.STANDARD);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Automation Tester","Sheiza Irfan Qureshi");
		extent.setSystemInfo("OS", "MacOS");
		extent.setSystemInfo("Environment", "Stagging");
		extent.setSystemInfo("Build No", "TajPatient_01-Dec-2020");	
	}

	@AfterSuite
	public void reportTearDown() {
		extent.flush();
	}
}