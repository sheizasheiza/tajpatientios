package test;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class RegisterPatient extends BaseClass{
	
	
	public ExtentTest test;
	public String mobilenumber="500600700";
	 @Test (priority =0, enabled=true)
	  public void AcceptAlert()
	  
	  {
		  test =extent.createTest("Allow Application Alert","");
			test.log(Status.INFO,"Test case to Allow Application to Install");
		  HashMap<String, String> param = new HashMap<String, String>();
			param.put("action", "getButtons");
			List<String> buttons = (List<String>) driver.executeScript("mobile: alert", param);		
			for(String button: buttons) {
				
				//System.out.println(button);
				if(button.equals("Allow")) {
					param.put("action", "accept");
					driver.executeScript("mobile: alert", param);
					break;
					
				}
			}
			
				test.log(Status.PASS,"Alert has been allowed");
	  }
	  
	  @Test (priority =1, enabled=true)
public void SelectLanguage() {
		  test =extent.createTest("Select App language","Test Case will select application language");
			test.log(Status.INFO,"Test case Started to Select Language");
		// MobileElement languageAr = driver.findElement(MobileBy.AccessibilityId("ar app lang unselec"));
		// languageAr.click();
		//	test.log(Status.PASS,"Clicked on arabic");
		 MobileElement languageSelection = driver.findElement(MobileBy.AccessibilityId("continue"));
		  languageSelection.click();
		  test.log(Status.PASS,"Clicked on Continue Button on language Screen");
      	  }
	  
	  @Test (priority=2, enabled=true)
	  public void EnterMobileNumber() {
		  test =extent.createTest("Enter Mobile Number","Test Case for Entering mobile number");
			test.log(Status.INFO,"Test case for Entering mobile number");
		 MobileElement inputNumber = driver.findElement(MobileBy.AccessibilityId("Enter Mobile Number"));
	         //WebElement inputNumber = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/"+
	          //       "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	                 inputNumber.sendKeys("500600903");
	                 test.log(Status.PASS,"Mobile number Entered");
	                // WebElement EnterMobileNumber = driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"CONTINUE\"]"));
	                // EnterMobileNumber.click();
	                 MobileElement languageSelection =  driver.findElement(MobileBy.AccessibilityId("continue"));
	       		  languageSelection.click();
	       		test.log(Status.PASS,"Clicked on Continue button on mobile number Screen");
	  }
	  @Test (priority =3, enabled=true)
	  public void EnterOTP(){
		  test =extent.createTest("Enter OTP","Test Case for Entering OTP");
			test.log(Status.INFO,"Test case Started for entering OTP");
		 WebDriverWait wait = new WebDriverWait(driver,20);
		 String pin1xpath ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
				  "XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
		  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin1xpath)));
		WebElement otp1 = driver.findElement(By.xpath(pin1xpath)); 
		  otp1.sendKeys("0");
		  test.log(Status.PASS,"Zero entered");
		  String pin2xpath= "//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
				  "XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
		  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin2xpath)));
	         WebElement otp2 = driver.findElement(By.xpath(pin2xpath));
	         otp2.sendKeys("1");
	         String pin3xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	   	      	  "XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	          wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin3xpath)));	         
	         
	         WebElement otp3 = driver.findElement(By.xpath(pin3xpath));
	         otp3.sendKeys("0");
	         test.log(Status.PASS,"Zero entered");
	        String pin4xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	    	       		  "XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin4xpath)));
	         
	         WebElement otp4 = driver.findElement(By.xpath(pin4xpath));
	         otp4.sendKeys("1");
	         test.log(Status.PASS,"One entered");
	         String pin5xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	     	       	 "XCUIElementTypeCollectionView/XCUIElementTypeCell[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin5xpath)));
	        
	         WebElement otp5 = driver.findElement(By.xpath(pin5xpath));
	         otp5.sendKeys("0");
	         test.log(Status.PASS,"Zero entered");
	         String pin6xpath ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
		       		  "XCUIElementTypeCollectionView/XCUIElementTypeCell[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin6xpath)));
	         WebElement otp6 = driver.findElement(By.xpath(pin6xpath));
	         otp6.sendKeys("1");
	         test.log(Status.PASS,"One entered");
	         test.log(Status.PASS,"OTP has been entered successfully");
   }
	  @Test (priority=4, enabled=true)
	  public void EnterInsuranceID() {
		  test =extent.createTest("Enter OTP","Test Case for Entering Insurance ID");
			test.log(Status.INFO,"Test case Started for entering Insurance ID");
		  WebDriverWait wait = new WebDriverWait(driver,20);	  
		  WebElement otp5 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
         otp5.sendKeys("2171615020");
         test.log(Status.PASS,"Insurance ID entered successfully");
         
		  WebElement labletap = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"National ID\"]"));
        labletap.click();
        test.log(Status.PASS,"Keyboard Closed");
        // MobileElement checkbox = (MobileElement) driver.findElementsByAccessibilityId("unchkbox");
       // checkbox.click();
        
         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeButton[@name=\"unchkbox\"]")));
         WebElement checkbox =driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"unchkbox\"]"));
         checkbox.click();
         checkbox.click();
         test.log(Status.PASS,"Checkbox clicked");

	   WebElement NextButton = driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Next\"]"));
       NextButton.click();
       test.log(Status.PASS,"Next button clicked");
//      try {
//		Thread.sleep(5000);
//	} catch (InterruptedException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	  }
	  public void clickNextButton() { 

		   MobileElement NextButton = driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Next\"]"));
	      ActionMethods.WaitByvisibilityOfElement(NextButton,5000);
	      ActionMethods.tapByElement(NextButton);
	      test.log(Status.PASS, "Clicked on next button");	      
	  }
	  @Test (priority=5 , enabled=true)
	  public void VerifyDetails() throws InterruptedException {
			test =extent.createTest("Verify details","Verify Insurance details");
			test.log(Status.INFO,"Test case: Verify Details");
			Thread.sleep(5000);
			String text=driver.findElement(MobileBy.AccessibilityId("Your details has been verified")).getAttribute("value");
			SoftAssert assertion= new SoftAssert();
			test.log(Status.PASS,"Verified insurance details successfully");
			AssertJUnit.assertEquals(text, text.equals("Your details has been verified")||text.equals("YOUR DETAILS HAS BEEN VERIFIED"));
		//XCUIElementTypeStaticText[@name="Your details has been verified"]
			//Thread.sleep(5000);
		   clickNextButton();
	  }
	  @Test (priority=6, enabled=true)
	  public void SetDoctorLanguage() throws InterruptedException {
		
		//XCUIElementTypeStaticText[@name="Choose Doctor Language"]
		    test =extent.createTest("Select doctor Language","This test case will Select doctor Language");
			test.log(Status.INFO,"Test case: Select Doctor language");
			Thread.sleep(5000);
			//String text=driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Your details has been verified\"]")).getText();
			//SoftAssert assertion= new SoftAssert();
			//test.log(Status.PASS,"Verified insurance details successfully");
			//assertion.assertEquals(text, text.equals("Choose Doctor Language")||text.equals("CHOOSE DOCTOR LANGUAGE"));
			
		//	String chooseLangxpath = "//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
			//"XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther\"";		 
			
			try {
	String cc="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]";
	MobileElement selectLanguage = driver.findElement(By.xpath(cc));
	ActionMethods.WaitByvisibilityOfElement(selectLanguage, 5000);
    ActionMethods.tapByElement(selectLanguage);   
    test.log(Status.PASS, "Language selected");
    clickNextButton();
			}
			catch(Exception e) {
				clickNextButton();
				
			}
	  }

	  @Test (priority=7, enabled=true)
	  public void SelectDoctorGengder() throws InterruptedException {
		  
		//XCUIElementTypeStaticText[@name="Select Doctor Gender"]
		  test =extent.createTest("Select doctor gender","This test case will Select doctor Gender");
			test.log(Status.INFO,"Test case: Select Doctor Gender");
			Thread.sleep(5000);
		 //String doctorGenderXpath= "//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]";	 
		// menu lang= 	
			//XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]
			try{
			String cc1="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeImage";
         MobileElement gender =driver.findElement(By.xpath("cc1"));
         ActionMethods.WaitByvisibilityOfElement(gender, 5000);
     	 ActionMethods.tapByElement(gender);
        //  gender.click();
          test.log(Status.PASS, "Doctor language selected");
		clickNextButton();
		}
		catch(Exception ex) {
			clickNextButton();
		}
	  }
			//pin
	  @Test (priority=8, enabled=true)
          public void createPin(){ 
        	 
        //	WebElement tapElement = driver.findElement(MobileBy.AccessibilityId("New PIN"));
		  test =extent.createTest("Create PIN","This test case will create new PIN");
		  test.log(Status.INFO,"Test case: Create PIN");	
            String pin1xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
    		MobileElement pin1 = driver.findElement(By.xpath(pin1xpath));
    		ActionMethods.WaitByvisibilityOfElement(pin1, 5000);
           pin1.sendKeys("1");
           String pin2xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
           
           MobileElement pin2 = driver.findElement(By.xpath(pin2xpath));
           ActionMethods.WaitByvisibilityOfElement(pin2, 5000);
           pin2.sendKeys("1");
           String pin3xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
           MobileElement pin3 = driver.findElement(By.xpath(pin3xpath));
           ActionMethods.WaitByvisibilityOfElement(pin3, 5000);
           pin3.sendKeys("1");
           String pin4xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
           
           MobileElement pin4 = driver.findElement(By.xpath(pin4xpath));
           ActionMethods.WaitByvisibilityOfElement(pin4, 5000);
           pin4.sendKeys("1");           
           WebElement newpintag = driver.findElement(MobileBy.AccessibilityId("New PIN"));
           newpintag.click();
           String pin5xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
           
           MobileElement pin5 = driver.findElement(By.xpath(pin5xpath));
           ActionMethods.WaitByvisibilityOfElement(pin5, 5000);
           pin5.sendKeys("1");
           String pin6xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
          
           MobileElement pin6 = driver.findElement(By.xpath(pin6xpath));
           ActionMethods.WaitByvisibilityOfElement(pin6, 5000);
           pin6.sendKeys("1");
           String pin7xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
          
           MobileElement pin7 = driver.findElement(By.xpath(pin7xpath));
           ActionMethods.WaitByvisibilityOfElement(pin7, 5000);
           pin7.sendKeys("1");
           String pin8xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
           
           MobileElement pin8 = driver.findElement(By.xpath(pin8xpath));
           ActionMethods.WaitByvisibilityOfElement(pin8, 5000);
           pin8.sendKeys("1");
         //XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField
         //XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField
           test.log(Status.PASS, "Pin created successfully");
           clickNextButton();
          }
	  @Test (priority=9,enabled=true)
          public void setSecqurityQuestion() {
		    //seq question
		  test =extent.createTest("Set security question","This test case will Set secret question");
		  test.log(Status.INFO,"Test case: Set secret question");
		String secQuestion= "//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
		//XCUIElementTypeButton[@name="Cancel"]
          MobileElement answer =driver.findElement(By.xpath(secQuestion));
          ActionMethods.WaitByvisibilityOfElement(answer, 5000);
          answer.sendKeys("abc");
          test.log(Status.PASS, "Answer Enered");
		  String btnDonexpath= "//XCUIElementTypeButton[@name=\"Done\"]";
         // wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnDonexpath)));
          MobileElement btnDone =driver.findElement(By.xpath(btnDonexpath));
          ActionMethods.WaitByvisibilityOfElement(btnDone, 5000);
          ActionMethods.tapByElement(btnDone);
          //btnDone.click();
          test.log(Status.PASS, "Keyboard closed");
          clickNextButton();
	  }

	  @Test  (priority=10, enabled=true)
	  public void HomeScreen() {
		  try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  }
} 